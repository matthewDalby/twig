console.log(groupArrayElements([1,2,3,4,5,6,7,8,9,10,11],3))

/* there is a large edge case where this doesn't function to the letter of the questions where the array size +1 divided by N is less than 2
*  this is due to the issue i have with the remainder clause, if there are 10 elements in the array and N = 6 then 6 goes in once with 4 remainder 
*  but this means that the group size is too small to host them all so if i increase the group size from 1 to 2 then 10/2 = 5 perfectly meaning that 
*  the last array is empty because now the remainder is 0
*/
function groupArrayElements(originalArray , groupNumber) {
var groupSize = originalArray.length/groupNumber;	
var remainder = originalArray.length%groupNumber
	if (remainder > 0) 
	{
		 groupSize++;
	}
	var returnArray = [];
	while (originalArray.length) returnArray.push(originalArray.splice(0,groupSize))

	return returnArray;
}